package com.dvios.rest.seed;

import com.dvios.rest.Util.MongoUtil;
import com.dvios.rest.handlers.hash.HashGenerationException;
import org.junit.Test;
import org.springframework.data.mongodb.core.MongoOperations;

import java.io.IOException;

/**
 * @author Dulaj
 * @version 1.0.0
 */
public abstract class AbstractSeeder {

    MongoOperations mongoOperation = MongoUtil.getOperator();

    @Test
    public abstract void seed() throws HashGenerationException, IOException;
}

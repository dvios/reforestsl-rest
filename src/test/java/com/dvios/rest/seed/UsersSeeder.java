package com.dvios.rest.seed;

import com.dvios.rest.handlers.hash.HashGenerationException;
import com.dvios.rest.handlers.hash.HashGenerationHandler;
import com.dvios.rest.models.collections.User;

/**
 * @author Dulaj
 * @version 1.0.0
 */
public class UsersSeeder extends AbstractSeeder {

    @Override
    public void seed() throws HashGenerationException {
//        mongoOperation.save(new User(User.UserLevel.PUBLIC, "Dulaj", "Atapattu", "dulaj.r.atapattu@gmail.com", "0715944191", "180, Bandaranayake Mawatha, Katubedda", HashGenerationHandler.hash("dulaj1234")));
//        mongoOperation.save(new User(User.UserLevel.PUBLIC, "Anuradha", "Wickramarachchi", "anuradhawick@gmail.com", "0712165724", "123, Ganemulla Road, Kadawatha", HashGenerationHandler.hash("anuradha1234")));
//        mongoOperation.save(new User(User.UserLevel.PUBLIC, "Ravidu", "Lashan", "ravidu@gmail.com", "0712755777", "101, Pirivena Road, Battaramulla", HashGenerationHandler.hash("ravidu1234")));
    }
}

package com.dvios.rest.seed;

import com.dvios.rest.handlers.hash.HashGenerationException;
import com.dvios.rest.models.collections.Post;
import com.dvios.rest.models.collections.User;
import com.dvios.rest.models.foundation.Location;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.io.IOException;
import java.util.Calendar;

/**
 * @author Dulaj
 * @version 1.0.0
 */
public class PostsSeeder extends AbstractSeeder{

    @Override
    public void seed() throws HashGenerationException, IOException {
//        Post post = new Post();
//        post.setTitle("Best fertilizers for flowers.");
//        post.setDescription("This post contains a list of fertilizers to use for flower plants.");
//        post.setTime(Calendar.getInstance().getTime());
//        post.setLocation(new Location("University of Moratuawa", "6.796994, 79.901767"));
//        post.setState(Post.State.SAVED);
//        post.setPostedById(mongoOperation.findOne(new Query(Criteria.where("email").is("dulaj.r.atapattu@gmail.com")), User.class).getId());
//        mongoOperation.save(post);
//
//        post = new Post();
//        post.setTitle("How to plant a coconut tree");
//        post.setDescription("This is a detailed guide about planting a coconut tree.");
//        post.setTime(Calendar.getInstance().getTime());
//        post.setLocation(new Location("Sinharaja", "6.391256, 80.497955"));
//        post.setState(Post.State.PUBLISHED);
//        post.setPostedById(mongoOperation.findOne(new Query(Criteria.where("email").is("anuradhawick@gmail.com")), User.class).getId());
//        mongoOperation.save(post);
//
//        post = new Post();
//        post.setTitle("Reforestration growth if Sri Lanka");
//        post.setDescription("This is a detailed guide about planting trees. ");
//        post.setTime(Calendar.getInstance().getTime());
//        post.setLocation(new Location("Yala", "6.279966, 81.401449"));
//        post.setState(Post.State.PUBLISHED);
//        post.setPostedById(mongoOperation.findOne(new Query(Criteria.where("email").is("ravidu@gmail.com")), User.class).getId());
//        mongoOperation.save(post);
    }
}

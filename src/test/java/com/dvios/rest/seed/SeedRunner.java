package com.dvios.rest.seed;

import com.dvios.rest.handlers.hash.HashGenerationException;

import java.io.IOException;

/**
 * @author Dulaj
 * @version 1.0.0
 */
public class SeedRunner {

    public static void main(String[] args) throws HashGenerationException, IOException {
        new UsersSeeder().seed();
        new PostsSeeder().seed();
        new EventSeeder().seed();
    }
}

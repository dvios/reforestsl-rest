package com.dvios.rest.seed;

import com.dvios.rest.handlers.hash.HashGenerationException;
import com.dvios.rest.models.collections.Event;
import com.dvios.rest.models.collections.User;
import com.dvios.rest.models.foundation.Location;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.io.IOException;
import java.util.Calendar;

/**
 * @author Dulaj
 * @version 1.0.0
 */
public class EventSeeder extends AbstractSeeder {

    @Override
    public void seed() throws HashGenerationException, IOException {
        Event event = new Event();
        event.setTitle("Reforestration campaign");
        event.setDescription("This is a annual event organized by Reforest Sri Lanka");
        event.setPostedById(mongoOperation.findOne(new Query(Criteria.where("email").is("ravidu@gmail.com")), User.class).getId());
        event.setState(Event.State.PUBLISHED);
        Calendar calendar = Calendar.getInstance();
        event.setStartDate(calendar.getTime());
        calendar.add(Calendar.HOUR, 20);
        event.setEndDate(calendar.getTime());
        event.setLocation(new Location("Sinharaja", "6.391256, 80.497955"));
    }
}

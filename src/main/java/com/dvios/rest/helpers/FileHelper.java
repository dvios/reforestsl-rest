package com.dvios.rest.helpers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.*;

/**
 * Created by anuradhawick on 12/14/16.
 */
public class FileHelper {

    public static boolean writeFile(InputStream in, String path) {
        File file = new File(path);

        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {

            if (!file.exists()) {
                file.createNewFile();
            }

            fileOutputStream.write(IOUtils.toByteArray(in));
            fileOutputStream.flush();
            fileOutputStream.close();

            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteFile(String path) {
        File file = new File(path);


        if (file.exists()) {
            file.delete();
        }


        return false;
    }
}

package com.dvios.rest.helpers;

import com.dvios.rest.config.InitialConfigurations;
import com.sun.mail.smtp.SMTPTransport;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

/**
 * Created by anuradhawick on 11/12/16.
 */
public class EmailHelper {
    private static String footer =
            "<br><br>\n" +
                    "<div style=\"text-align: center; font-family: Helvetica, Arial, Freesans, Clean, Sans-serif;\">\n" +
                    "    <div style=\"display: inline-block;\">\n" +
                    "        <span style=\"background-color: #f44842;\n" +
                    "                    border: none;\n" +
                    "                    border-radius: 3px;\n" +
                    "                    color: white;\n" +
                    "                    padding: 5px 10px;\n" +
                    "                    text-align: center;\n" +
                    "                    text-decoration: none;\n" +
                    "                    display: inline-block;\n" +
                    "                    font-size: 16px;\">Powered by:\n" +
                    "            <a href=\"https://www.facebook.com/TeamDVios\"><img\n" +
                    "                    style=\"max-height: 30px;\n" +
                    "                display: inline-block;\n" +
                    "                vertical-align: middle;\n" +
                    "                border-radius: 3px\"\n" +
                    "                    src=\"" + InitialConfigurations.API_HOST + "/assets/dvios.jpg\"\n" +
                    "                    alt=\"\"></a>\n" +
                    "        </span>\n" +
                    "    </div>\n" +
                    "\n" +
                    "</div>";

    public static void sendPasswordResetLink(String email, String key) {
        String content = "<div style=\"font-family: Helvetica, Arial, Freesans, Clean, Sans-serif;\">\n" +
                "    <div>\n" +
                "        <img style=\"display: block; margin: 0 auto; width: 100%;  max-width: 800px;\"\n" +
                "             src=\"" + InitialConfigurations.API_HOST +
                "/assets/reforest-logo.png\"\n" +
                "             alt=\"\">\n" +
                "    </div>\n" +
                "    <div>\n" +
                "        <h1 style=\"text-align: center;\">Reforest Sri Lanka</h1>\n" +
                "        <div style=\"text-align: center\">\n" +
                "            Follow the below link to reset your password.\n" +
                "            <a\n" +
                "                    style=\"background-color: #4CAF50;\n" +
                "                    border: none;\n" +
                "                    border-radius: 3px;\n" +
                "                    color: white;\n" +
                "                    padding: 5px 10px;\n" +
                "                    text-align: center;\n" +
                "                    text-decoration: none;\n" +
                "                    display: inline-block;\n" +
                "                    font-size: 16px;\"\n" +
                "                    type=\"button\"\n" +
                "                    href=\"" + InitialConfigurations.API_HOST + "/rest/user/reset-password?key=" + key + "\" target=\"_blank\">CLICK HERE\n" +
                "            </a>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>" +
                footer;
        sendEmail(email, content);
    }

    public static void sendNewPassword(String email, String password) {
        String content = "<div style=\"font-family: Helvetica, Arial, Freesans, Clean, Sans-serif;\">\n" +
                "    <div>\n" +
                "        <img style=\"display: block; margin: 0 auto; width: 100%; max-width: 800px\"\n" +
                "             src=\"" + InitialConfigurations.API_HOST +
                "/assets/reforest-logo.png\"\n" +
                "             alt=\"\">\n" +
                "    </div>\n" +
                "    <div>\n" +
                "        <h1 style=\"text-align: center;\">Reforest Sri Lanka</h1>\n" +
                "        <div style=\"text-align: center\">\n" +
                "            Your password has been set to: <span\n" +
                "                style=\"background-color: #4CAF50;\n" +
                "                    border: none;\n" +
                "                    border-radius: 3px;\n" +
                "                    color: white;\n" +
                "                    padding: 5px 10px;\n" +
                "                    text-align: center;\n" +
                "                    text-decoration: none;\n" +
                "                    display: inline-block;\n" +
                "                    font-size: 16px;\"\n" +
                "        >" +
                password +
                "            </span> login with this password and reset from the Reforest mobile app.\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>" +
                footer;
        sendEmail(email, content);
    }

    private static void sendEmail(String email, String body) {
        final String server = "server1.et.lk";
        final String username = "noreply@reforestsrilanka.com";
        final String password = "reforest@noreply";

        Properties props = new Properties();
        props.put("mail.smtps.auth", "true");
        props.put("mail.smtps.starttls.enable", "true");
        props.put("mail.smtps.host", "server1.et.lk");
        props.put("mail.smtps.port", "465");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        Message message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress("noreply@reforestsrilanka.com", "Reforest Sri Lanka"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            message.setSubject("Reforest Sri Lanka - Member help");
            message.setContent(body, "text/html; charset=utf-8");
            SMTPTransport t =
                    (SMTPTransport) session.getTransport("smtps");
            t.connect(server, username, password);
            t.sendMessage(message, message.getAllRecipients());
            t.close();
        } catch (MessagingException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}

package com.dvios.rest.helpers;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.files.WriteMode;
import com.dropbox.core.v2.sharing.SharedLinkMetadata;
import com.dvios.rest.config.InitialConfigurations;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by anuradhawick on 10/27/16.
 */
public class DbxHelper {

    public static String uploadToDbx(InputStream in, String path, String fileName) throws DbxException, IOException {
        DbxRequestConfig config = DbxRequestConfig.newBuilder("reforest-sl").build();
        DbxClientV2 client = new DbxClientV2(config, InitialConfigurations.DBX_ACCESS_TOKEN);

        String fullPath = InitialConfigurations.DBX_MAIN_PATH + path + fileName;

        FileMetadata metadata = client.files().uploadBuilder(fullPath).withMode(WriteMode.ADD).uploadAndFinish(in);

        SharedLinkMetadata sharedLinkMetadata = client.sharing().createSharedLinkWithSettings(fullPath);
        String url = sharedLinkMetadata.getUrl();

        url = url.split("\\?")[0] + "?raw=1";
        return url;
    }

    public static boolean updateDbx(InputStream in, String path, String fileName) {
        try {
            DbxRequestConfig config = DbxRequestConfig.newBuilder("reforest-sl").build();
            DbxClientV2 client = new DbxClientV2(config, InitialConfigurations.DBX_ACCESS_TOKEN);
            String fullPath = InitialConfigurations.DBX_MAIN_PATH + path + fileName;
            FileMetadata metadata = client.files().uploadBuilder(fullPath).withMode(WriteMode.OVERWRITE).uploadAndFinish(in);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    // test cases
//    public static void main(String[] args) {
//        try (InputStream in = new FileInputStream("/Users/anuradhawick/Desktop/test/test/src/main/java/test.txt")) {
////            String s = uploadToDbx(in, "/files/", "testedname.txt");
////            String s = uploadToDbx(in, "/files/", "testedname.txt");
////            System.out.println(s);
////            System.out.println(updateDbx(in,"/files/", "testedname.txt"));
//            System.out.println(deleteFromDbx("/files/", "testedname.txt"));
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();}
////        } catch (DbxException e) {
////            e.printStackTrace();
////        }
//    }

    public static boolean deleteFromDbx(String path, String fileName) {
        try {
            DbxRequestConfig config = DbxRequestConfig.newBuilder("reforest-sl").build();
            DbxClientV2 client = new DbxClientV2(config, InitialConfigurations.DBX_ACCESS_TOKEN);
            String fullPath = InitialConfigurations.DBX_MAIN_PATH + path + fileName;
            Metadata metadata = client.files().delete(fullPath);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }
}

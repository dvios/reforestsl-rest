package com.dvios.rest.ActivityLogger;

import com.dvios.rest.Util.MongoUtil;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Calendar;
import java.util.Date;

/**
 * @author anuradhawick
 * @version 1.0.0
 */
@Document(collection = "user_activity")
public class Activity {
    @Id
    private String _id;
    private Action action;
    private Date timestamp;
    private String postedById;
    private Object actionObject;

    private Activity() {
    }

    public static void stampActivity(Action action, String posttedById) {
        Activity activity = new Activity();
        activity.setAction(action);
        activity.setTimestamp(Calendar.getInstance().getTime());
        activity.setPostedById(posttedById);
        activity.setActionObject(null);
        MongoUtil.getOperator().save(activity);
    }

    public static void stampActivity(Action action, String postedById, Object actionObject) {
        Activity activity = new Activity();
        activity.setAction(action);
        activity.setTimestamp(Calendar.getInstance().getTime());
        activity.setPostedById(postedById);
        activity.setActionObject(actionObject);
        MongoUtil.getOperator().save(activity);
    }

    public Action getAction() {
        return action;
    }

    private void setAction(Action action) {
        this.action = action;
    }

    public String get_id() {
        return _id;
    }

    private void set_id(String _id) {
        this._id = _id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    private void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getPostedById() {
        return postedById;
    }

    private void setPostedById(String postedById) {
        this.postedById = postedById;
    }

    public Object getActionObject() {
        return actionObject;
    }

    private void setActionObject(Object actionObject) {
        this.actionObject = actionObject;
    }
}

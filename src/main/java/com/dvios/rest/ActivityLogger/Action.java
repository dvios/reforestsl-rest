package com.dvios.rest.ActivityLogger;

/**
 * @author anuradhawick
 * @version 1.0.0
 */
public enum Action {
    LOGIN,
    LOGOUT,
    REGISTER,
    POST_COMPLAINT,
    POST_EVENT,
    POST_PLANT
}

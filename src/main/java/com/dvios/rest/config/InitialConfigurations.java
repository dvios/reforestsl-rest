package com.dvios.rest.config;

/**
 * @author dulaj
 * @version 1.0.0
 */
public class InitialConfigurations {

    public static final String API_HOST = "http://45.55.94.191:8080";
//        public static final String API_HOST = "http://localhost:9000";
    public static final String HASH_ALGORITHM = "SHA-256";
    public static final String JWT_SECRET = "DVios@1234";
    public static final int PAGINATION_PAGE_SIZE = 10;
    public static final int PAGINATION_PAGE_SIZE_LISTS = 25;

    public static final String PUSHER_APP_ID = "254878";
    public static final String PUSHER_APP_KEY = "c86a178b30fe38076955";
    public static final String PUSHER_APP_SECRET = "e202cc29d508c0d9abe5";

    public static final String DBX_ACCESS_TOKEN = "-VP2on4cSIAAAAAAAAAACD2n25l5Z63da48Xo8t2O7BIISrRMZJVOJk96gD3-tsk";
    public static final String DBX_MAIN_PATH = "";
    public static final String DBX_USER_IMAGE_PATH = "/users/";
    public static final String DBX_COMPLAINT_IMAGES_PATH = "/complaints/";
    public static final String DBX_EVENT_IMAGES_PATH = "/events/";
    public static final String DBX_POST_IMAGES_PATH = "/posts/";


//    private static final String IMAGES_PATH = "/Users/anuradhawick/Desktop/IdeaRest/reforestsl-rest/src/main/webapp/images";
    // to be used by server
    private static final String IMAGES_PATH = "/root/reforestsl-rest/src/main/webapp/images";

    public static final String USER_IMAGE_PATH = IMAGES_PATH + "/users/";
    public static final String POST_IMAGES_PATH = IMAGES_PATH + "/posts/";
    public static final String EVENT_IMAGES_PATH = IMAGES_PATH + "/events/";
    public static final String COMPLAINT_IMAGES_PATH = IMAGES_PATH + "/complaints/";

    public static final String IMAGES_URL_PATH = API_HOST + "/images/";

}

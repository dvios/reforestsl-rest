package com.dvios.rest.handlers;

import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.JWTVerifier;
import com.dvios.rest.config.InitialConfigurations;
import com.dvios.rest.models.enums.Role;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.SignatureException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dulaj
 * @version 1.0.0
 */
public class TokenHandler {

    private static final Logger logger = LogManager.getLogger();

    public static String generateJWT(String sub, List<Role> roles) {
        final String issuer = "dvios";
        final String secret = InitialConfigurations.JWT_SECRET;
        final JWTSigner signer = new JWTSigner(secret);
        final HashMap<String, Object> claims = new HashMap<>();
        String rolesString = "";
        for (Role role : roles) {
            rolesString += role.toString() + " ";
        }
        rolesString = rolesString.trim();
        claims.put("iss", issuer);
        claims.put("aud", sub);
        claims.put("roles", rolesString);

        return signer.sign(claims);
    }

    public static Map<String, Object> validateJWT(String token) throws SignatureException {
        final String secret = InitialConfigurations.JWT_SECRET;
        try {
            final JWTVerifier verifier = new JWTVerifier(secret);
            final Map<String, Object> claims = verifier.verify(token);
            return claims;
        } catch (Exception e) {
            throw new SignatureException("Token verification failed");
        }
    }
}

package com.dvios.rest.handlers.hash;

/**
 * @author dulaj
 * @version 1.0.0
 */
public class HashGenerationException extends Exception {

    public HashGenerationException() {
    }

    public HashGenerationException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public HashGenerationException(String message) {
        super(message);
    }

    public HashGenerationException(Throwable throwable) {
        super(throwable);
    }

}

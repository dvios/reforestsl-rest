package com.dvios.rest.handlers.hash;

import com.dvios.rest.config.InitialConfigurations;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author dulaj
 * @version 1.0.0
 */
public class HashGenerationHandler {

    public static String hash(String message) throws HashGenerationException {
        try {
            MessageDigest ex = MessageDigest.getInstance(InitialConfigurations.HASH_ALGORITHM);
            return new String(ex.digest(message.getBytes("UTF-8")), "UTF-8");
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new HashGenerationException("Could not generate hash from String", e);
        }
    }

    public static boolean match(String message, String hash) throws HashGenerationException {
        return hash.equals(hash(message));
    }
}

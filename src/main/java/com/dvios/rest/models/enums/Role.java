package com.dvios.rest.models.enums;

/**
 * Created by anuradhawick on 12/26/16.
 */
public enum Role {
    ADMIN,
    SUPER_ADMIN,
}

package com.dvios.rest.models.collections;

import com.dvios.rest.models.foundation.Location;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Dulaj
 * @version 1.0.0
 *          Event-> Start-End date and times, title, description, photos, locations,
 */

@Document(collection = "events")
public class Event {

    @Id
    private String id;
    private State state = State.SAVED;
    private String title;
    private String description;
    private Date startDate;
    private Date endDate;
    private List<String> images;
    private Location location;
    private String postedById;
    private Date timestamp;

    public Event() {
        this.timestamp = Calendar.getInstance().getTime();
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public enum State {
        SAVED, PUBLISHED
    }


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public State getState() {
        return state;
    }

    public String getTitle() {
        return title;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getPostedById() {
        return postedById;
    }

    public void setPostedById(String postedById) {
        this.postedById = postedById;
    }
}

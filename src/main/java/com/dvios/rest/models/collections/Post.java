package com.dvios.rest.models.collections;

import com.dvios.rest.models.foundation.Location;
import com.owlike.genson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author dulaj
 * @version 1.0.0
 *          Post -> Title, Description
 *          (Show username on wall and timestamp), Photo/photos,
 *          date and time of post, Location, Not necessary for Date and time of
 *          activity if any because even on FB we dont see that option now
 */

@Document(collection = "posts")
public class Post {

    @Id
    private String id;
    private String title;
    private String description;
    private State state = State.SAVED;
    private String postedById;
    private Location location;
    private List<String> images;
    private Date timestamp;

    public Post() {
        this.timestamp = Calendar.getInstance().getTime();
    }

    public Post(@JsonProperty("location") Location location) {
        super();
        this.location = location;
        this.timestamp = Calendar.getInstance().getTime();
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public enum State {
        SAVED, PUBLISHED
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public State getState() {
        return state;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getPostedById() {
        return postedById;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void setPostedById(String postedById) {
        this.postedById = postedById;
    }
}

package com.dvios.rest.models.collections;

import com.dvios.rest.models.enums.Role;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author dulaj
 * @version 1.0.0
 */

@Document(collection = "users")
public class User {

    @Id
    private String id;
    private UserLevel userLevel;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String address;
    private String password;
    private String image;
    private Date timestamp;
    private List<Role> roles = new ArrayList<>();

    public User() {
        this.timestamp = Calendar.getInstance().getTime();
    }

    public User(UserLevel userLevel, String firstName, String lastName, String email, String phone, String address, String password) {
        this.userLevel = userLevel;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.password = password;
        this.timestamp = Calendar.getInstance().getTime();
    }

    public String getId() {
        return id;
    }

    public UserLevel getUserLevel() {
        return userLevel;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public String getPassword() {
        return password;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserLevel(UserLevel userLevel) {
        this.userLevel = userLevel;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public enum UserLevel {
        ADMIN, REFORESTSL, PUBLIC;

        int value;

        UserLevel() {
            value = ordinal();
        }

        public int getValue() {
            return value;
        }

        public static UserLevel getUserLevelByValue(int value) {
            return UserLevel.values()[value];
        }
    }
}

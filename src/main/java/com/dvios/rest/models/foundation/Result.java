package com.dvios.rest.models.foundation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.util.HashMap;

/**
 * @author dulaj
 * @version 1.0.0
 */
public class Result {

    private boolean success;
    private String error;
    private HashMap<String, Object> data = new HashMap<>();

    public boolean isSuccess() {
        return success;
    }

    public String getError() {
        return error;
    }

    public Object getData() {
        return data;
    }

    public void setAsSuccess() {
        this.success = true;
        this.error = null;
    }

    public void setAsError(String error) {
        this.success = false;
        this.error = error;
    }

    public void addData(String key, Object value) {
        data.put(key, value);
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setDateFormat("YYYY-MM-dd hh:mm a").create();
        JsonObject jsonObject = (JsonObject) gson.toJsonTree(this);
        if (error == null) {
            jsonObject.remove("error");
        }
        if (data.size() == 0) {
            jsonObject.remove("data");
        }
        return jsonObject.toString();
    }
}

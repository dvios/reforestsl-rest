package com.dvios.rest.models.foundation;

/**
 * @author Dulaj
 * @version 1.0.0
 */

public class Location {

    private String name;
    private String coordinates;

    public Location() {
    }

    public Location(String name, String coordinates) {
        this.name = name;
        this.coordinates = coordinates;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }
}

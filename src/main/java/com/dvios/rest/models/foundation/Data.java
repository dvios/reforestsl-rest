package com.dvios.rest.models.foundation;

/**
 * Created by anuradhawick on 11/1/16.
 */
public class Data<T> {
    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}

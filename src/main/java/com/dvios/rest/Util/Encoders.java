package com.dvios.rest.Util;


import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.UUID;

/**
 * Created by anuradhawick on 10/27/16.
 */
public class Encoders {

    public static InputStream base64toBinary(String data) {
        String imageDataBytes = data.substring(data.indexOf(",") + 1);

        return new ByteArrayInputStream(com.auth0.jwt.internal.org.bouncycastle.util.encoders.
                Base64.decode(imageDataBytes));
    }

    public static String stringToBase64(String string) {
        byte[] bytesEncoded = org.apache.commons.codec.binary.Base64.encodeBase64(string.getBytes());
        return new String(bytesEncoded);
    }

}

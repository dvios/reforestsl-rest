package com.dvios.rest.Util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;

/**
 * @author anuradhawick
 * @version 1.0.0
 */
public class MongoUtil {
    private static MongoOperations operations = null;

    public static MongoOperations getOperator() {
        if (operations == null) {
            synchronized (MongoUtil.class) {
                if (operations == null) {
                    ApplicationContext ctx = new GenericXmlApplicationContext("spring.xml");
                    operations = (MongoOperations) ctx.getBean("mongoTemplate");
                }
            }
        }
        return operations;
    }
}

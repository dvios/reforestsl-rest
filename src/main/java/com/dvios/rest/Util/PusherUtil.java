package com.dvios.rest.Util;

import com.dvios.rest.config.InitialConfigurations;
import com.pusher.rest.Pusher;

/**
 * @author anuradhawick
 * @version 1.0.0
 */
public class PusherUtil {
    private static Pusher pusher = null;

    public static Pusher getOperator() {
        if (pusher == null) {
            synchronized (PusherUtil.class) {
                if (pusher == null) {
                    pusher = new Pusher(InitialConfigurations.PUSHER_APP_ID,
                            InitialConfigurations.PUSHER_APP_KEY,
                            InitialConfigurations.PUSHER_APP_SECRET);
                    pusher.setEncrypted(true);
                    pusher.setRequestTimeout(10000);
                }
            }
        }
        return pusher;
    }
}

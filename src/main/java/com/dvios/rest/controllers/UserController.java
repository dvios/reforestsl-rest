package com.dvios.rest.controllers;

import com.dropbox.core.DbxException;
import com.dvios.rest.ActivityLogger.Action;
import com.dvios.rest.ActivityLogger.Activity;
import com.dvios.rest.Util.Encoders;
import com.dvios.rest.config.InitialConfigurations;
import com.dvios.rest.controllers.auth.Secured;
import com.dvios.rest.handlers.TokenHandler;
import com.dvios.rest.handlers.hash.HashGenerationException;
import com.dvios.rest.handlers.hash.HashGenerationHandler;
import com.dvios.rest.helpers.DbxHelper;
import com.dvios.rest.helpers.EmailHelper;
import com.dvios.rest.helpers.FileHelper;
import com.dvios.rest.models.collections.PasswordResets;
import com.dvios.rest.models.collections.User;
import com.dvios.rest.models.enums.Role;
import com.dvios.rest.models.foundation.Result;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

/**
 * @author dulaj
 * @version 1.0.0
 */

@Path("user")
public class UserController extends AbstractController {

    private static final Logger logger = LogManager.getLogger();

    @PUT
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createNewUser(final User user, @QueryParam("method") String method) {
        Result result = new Result();
        Query query;

        switch (method) {
            case "google":
                // TODO add fields and implement google login
                break;
            default:
                try {
                    user.setPassword(HashGenerationHandler.hash(user.getPassword()));
                } catch (HashGenerationException e) {
                    logger.error("Error while hashing password", e);
                    return returnError(result, "Error in password");
                }

                query = new Query();
                query.addCriteria(Criteria.where("email").is(user.getEmail()));
                if (mongoOperation.findOne(query, User.class) != null) {
                    return returnError(result, "Email already registered");
                }

                query = new Query();
                if (user.getPhone().length() > 0) {
                    query.addCriteria(Criteria.where("phone").is(user.getPhone()));
                    if (mongoOperation.findOne(query, User.class) != null) {
                        return returnError(result, "Phone number already registered");
                    }
                }
                break;
        }
        // Process image
        if (user.getImage() != null && user.getImage().length() != 0) {
            InputStream in = Encoders.base64toBinary(user.getImage());
            String imagePath = InitialConfigurations.USER_IMAGE_PATH + user.getEmail() + ".jpg";
            user.setImage(InitialConfigurations.IMAGES_URL_PATH + "users/" + user.getEmail() + ".jpg");
            FileHelper.writeFile(in, imagePath);
        }
        mongoOperation.insert(user);

        result.setAsSuccess();
        Activity.stampActivity(Action.REGISTER, user.getId(), user);

        return Response.status(200).entity(result.toString()).build();
    }

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(User user, @QueryParam("method") String method) {
        Result result = new Result();
        Query query;

        query = new Query();
        query.addCriteria(Criteria.where("email").is(user.getEmail()));
        User userFromDB = mongoOperation.findOne(query, User.class);
        if (userFromDB == null) {
            return returnError(result, "Incorrect email");
        }

        try {
            if (HashGenerationHandler.match(user.getPassword(), userFromDB.getPassword())) {
                result.setAsSuccess();
                result.addData("token", TokenHandler.generateJWT(userFromDB.getId(), userFromDB.getRoles()));
                Activity.stampActivity(Action.LOGIN, userFromDB.getId());
                return Response.status(200).entity(result.toString()).build();
            } else {
                return returnError(result, "Incorrect password");
            }
        } catch (Exception | Error e) {
            e.printStackTrace();
            logger.error("Error while matching hash", e);
            return returnError(result, "Incorrect password");
        }
    }

    @GET
    @Secured
    @Path("/get")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserProfile(@Context SecurityContext securityContext) {
        Principal principal = securityContext.getUserPrincipal();
        String userId = principal.getName();
        Result result = new Result();

        Query query = new Query(Criteria.where("_id").is(new ObjectId(userId)));
        User user = mongoOperation.findOne(query, User.class);
        user.setPassword(null);
        result.setAsSuccess();
        result.addData("user", user);

        return Response.status(200).entity(result.toString()).build();
    }

    @POST
    @Secured
    @Path("/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUserProfile(@Context SecurityContext securityContext, User newUserData) {
        Principal principal = securityContext.getUserPrincipal();
        String userId = principal.getName();
        Result result = new Result();

        Query query = new Query(Criteria.where("_id").is(userId));
        final User user = mongoOperation.findOne(query, User.class);
        // if the name has changed
        if (newUserData.getFirstName() != null) {
            user.setFirstName(newUserData.getFirstName());
        }
        if (newUserData.getLastName() != null) {
            user.setLastName(newUserData.getLastName());
        }
        // if the image has changed
        if (newUserData.getImage() != null && !newUserData.getImage().equals("delete")) {
            // if the user has no image now
            InputStream in = Encoders.base64toBinary(newUserData.getImage());
            String imagePath = InitialConfigurations.USER_IMAGE_PATH + user.getEmail() + ".jpg";
            if (user.getImage() == null || user.getImage().length() == 0) {
                user.setImage(InitialConfigurations.IMAGES_URL_PATH + "users/" + user.getEmail() + ".jpg");
                FileHelper.writeFile(in, imagePath);
            } else {
                user.setImage(InitialConfigurations.IMAGES_URL_PATH + "users/" + user.getEmail() + ".jpg");
                FileHelper.writeFile(in, imagePath);
            }
        } else if (newUserData.getImage().equals("delete")) {
            user.setImage(null);
            FileHelper.deleteFile(InitialConfigurations.USER_IMAGE_PATH + user.getEmail() + ".jpg");
        }
        // if the phone number has changed
        if (newUserData.getPhone() != null) {
            user.setPhone(newUserData.getPhone());
        }
        // if the email number has changed
        if (newUserData.getEmail() != null) {
            user.setEmail(newUserData.getEmail());
        }
        // if the password has changed
        if (newUserData.getPassword() != null) {
            try {
                user.setPassword(HashGenerationHandler.hash(newUserData.getPassword()));
            } catch (HashGenerationException e) {
                // TODO decide on what to be sent to user
            }
        }

        mongoOperation.save(user);
        result.setAsSuccess();

        return Response.status(200).entity(result.toString()).build();
    }

    @GET
    @Path("/reset-password-request")
    @Produces(MediaType.APPLICATION_JSON)
    public Response resetPassword(@QueryParam("email") final String email) {
        Query query = new Query();
        query.addCriteria(Criteria.where("email").is(email));
        final User user = mongoOperation.findOne(query, User.class);
        if (user != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    PasswordResets passwordResets = new PasswordResets();
                    passwordResets.setRandomKey(UUID.randomUUID().toString());
                    passwordResets.setEmail(user.getEmail());
                    mongoOperation.save(passwordResets);
                    EmailHelper.sendPasswordResetLink(email, passwordResets.getRandomKey());
                }
            }).start();
        }
        Result result = new Result();
        result.setAsSuccess();
        return Response.status(Response.Status.OK).entity(result.toString()).build();
    }

    @GET
    @Path("/reset-password")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getNewPassword(@QueryParam("key") String key) throws URISyntaxException {
        Query query = new Query();
        query.addCriteria(Criteria.where("randomKey").is(key));
        PasswordResets passwordResets = mongoOperation.findOne(query, PasswordResets.class);
        if (passwordResets == null) {
            return Response.temporaryRedirect(new URI("/messages/reset-success.html")).build();
        }

        String password = Encoders.stringToBase64(UUID.randomUUID().toString()).substring(0, 8);

        String userEmail = passwordResets.getEmail();
        query = new Query();
        query.addCriteria(Criteria.where("email").is(userEmail));
        User user = mongoOperation.findOne(query, User.class);

        Result result = new Result();
        try {
            user.setPassword(HashGenerationHandler.hash(password));
            mongoOperation.save(user);
            mongoOperation.remove(passwordResets);
            result.setAsSuccess();
            EmailHelper.sendNewPassword(userEmail, password);
            return Response.temporaryRedirect(new URI("/messages/reset-success.html")).build();
        } catch (HashGenerationException e) {
            result.setAsError("Operation failed");
            return Response.temporaryRedirect(new URI("/messages/reset-success.html")).build();
        }
    }

    @GET
    @Path("/public-profile")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPublicProfile(@Context UriInfo uriInfo) {
        MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
        String userId = queryParams.getFirst("userId");
        User user;
        Result result = new Result();
        if (userId != null &&
                (user = mongoOperation.findOne(Query.query(Criteria.where("id").is(userId)), User.class)) != null) {
            result.setAsSuccess();
            result.addData("firstName", user.getFirstName());
            result.addData("lastName", user.getLastName());
        } else {
            result.setAsError("Not Found");
        }
        return Response.status(Response.Status.OK).entity(result.toString()).build();
    }
}
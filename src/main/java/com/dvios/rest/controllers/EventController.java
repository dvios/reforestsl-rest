package com.dvios.rest.controllers;

import com.dropbox.core.DbxException;
import com.dvios.rest.ActivityLogger.Action;
import com.dvios.rest.ActivityLogger.Activity;
import com.dvios.rest.Util.Encoders;
import com.dvios.rest.Util.PusherUtil;
import com.dvios.rest.config.InitialConfigurations;
import com.dvios.rest.controllers.auth.Secured;
import com.dvios.rest.handlers.TokenHandler;
import com.dvios.rest.helpers.DbxHelper;
import com.dvios.rest.helpers.FileHelper;
import com.dvios.rest.models.collections.Event;
import com.dvios.rest.models.collections.Post;
import com.dvios.rest.models.foundation.Result;
import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Dulaj
 * @version 1.0.0
 */

@Secured
@Path("event")
public class EventController extends AbstractController {

    @Context
    SecurityContext securityContext;

    private static final Logger logger = LogManager.getLogger();

    @PUT
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createEvent(String json) {
        Principal principal = securityContext.getUserPrincipal();
        String userId = principal.getName();
        Result result = new Result();

        Genson genson = new GensonBuilder()
                .useConstructorWithArguments(true)
                .useDateFormat(new SimpleDateFormat("yyyy-MM-dd hh:mm a"))
                .create();
        Event event = genson.deserialize(json, Event.class);

        event.setPostedById(userId);
        mongoOperation.insert(event);

        List<String> imagesUrls = new ArrayList<>();
        if (event.getImages() != null && event.getImages().size() > 0) {
            int index = 0;
            for (String image : event.getImages()) {
                InputStream in = Encoders.base64toBinary(image);
                String imagePath = InitialConfigurations.EVENT_IMAGES_PATH + event.getId() + "_" + index + ".jpg";
                FileHelper.writeFile(in, imagePath);
                imagesUrls.add(InitialConfigurations.IMAGES_URL_PATH + "events/" + event.getId() + "_" + index + ".jpg");
                index++;
            }
            event.setImages(imagesUrls);
            mongoOperation.save(event);
        }

        result.setAsSuccess();
        Activity.stampActivity(Action.POST_EVENT, userId, event);
        // TODO move this to a separate helper class after admin implementation of post approval
        PusherUtil.getOperator().trigger("test_channel", "my_event", Collections.singletonMap("message", "New event available"));
        return Response.status(200).entity(result.toString()).build();
    }

    @GET
    @Path("/get")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEventsByUser(@QueryParam("offset") int offset) {
        Principal principal = securityContext.getUserPrincipal();
        String userId = principal.getName();

        Result result = new Result();
        List<Event> eventList;

        Query query = new Query(Criteria.where("postedById").is(userId));
        query.skip(offset);
        query.limit(InitialConfigurations.PAGINATION_PAGE_SIZE);
        query.with(new Sort(Sort.Direction.DESC, "timestamp"));

        eventList = mongoOperation.find(query, Event.class);
        result.setAsSuccess();
        result.addData("offset", offset);
        result.addData("posts", eventList);

        return Response.status(200).entity(result.toString()).build();
    }

    @GET
    @Path("/get-all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllEvents(@QueryParam("offset") int offset) {
        Result result = new Result();
        List<Event> eventList;

        Query query = new Query();
        query.skip(offset);
        query.limit(InitialConfigurations.PAGINATION_PAGE_SIZE);
        query.with(new Sort(Sort.Direction.DESC, "timestamp"));

        eventList = mongoOperation.find(query, Event.class);
        result.setAsSuccess();
        result.addData("offset", offset);
        result.addData("posts", eventList);

        return Response.status(200).entity(result.toString()).build();
    }

    @SuppressWarnings("Duplicates")
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletePost(String json) {
        Genson genson = new GensonBuilder()
                .useConstructorWithArguments(true)
                .useDateFormat(new SimpleDateFormat("yyyy-MM-dd hh:mm a"))
                .create();
        Event event = genson.deserialize(json, Event.class);

        Event dbEvent = mongoOperation.findOne(Query.query(Criteria.where("_id").is(event.getId())), Event.class);

        // deleting all the images
        if (dbEvent != null && dbEvent.getImages() != null && dbEvent.getImages().size() > 0) {
            int index = 0;
            for (String image : dbEvent.getImages()) {
                String imagePath = InitialConfigurations.EVENT_IMAGES_PATH + dbEvent.getId() + "_" + index + ".jpg";
                FileHelper.deleteFile(imagePath);
                index++;
            }
        }

        Result result = new Result();

        mongoOperation.remove(event);
        result.setAsSuccess();
        return Response.status(200).entity(result.toString()).build();
    }

    @POST
    @Path("/edit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editPost(String json) {
        Result result = new Result();
        Genson genson = new GensonBuilder()
                .useConstructorWithArguments(true)
                .create();
        Event event = genson.deserialize(json, Event.class);

        mongoOperation.save(event);
        result.setAsSuccess();
        return Response.status(200).entity(result.toString()).build();
    }
}

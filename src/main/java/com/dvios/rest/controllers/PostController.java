package com.dvios.rest.controllers;

import com.dropbox.core.DbxException;
import com.dvios.rest.ActivityLogger.Action;
import com.dvios.rest.ActivityLogger.Activity;
import com.dvios.rest.Util.Encoders;
import com.dvios.rest.config.InitialConfigurations;
import com.dvios.rest.controllers.auth.Secured;
import com.dvios.rest.helpers.DbxHelper;
import com.dvios.rest.helpers.FileHelper;
import com.dvios.rest.models.collections.Post;
import com.dvios.rest.models.foundation.Result;
import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author dulaj
 * @version 1.0.0
 */

@Secured
@Path("post")
public class PostController extends AbstractController {

    @Context
    SecurityContext securityContext;

    private static final Logger logger = LogManager.getLogger();

    @PUT
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createNewPost(String json) {
        Principal principal = securityContext.getUserPrincipal();
        String userId = principal.getName();
        Result result = new Result();

        Genson genson = new GensonBuilder()
                .useConstructorWithArguments(true)
                .useDateFormat(new SimpleDateFormat("yyyy-MM-dd hh:mm a"))
                .create();
        Post post = genson.deserialize(json, Post.class);

        post.setPostedById(userId);
        mongoOperation.insert(post);

        List<String> imagesUrls = new ArrayList<>();
        if (post.getImages() != null && post.getImages().size() > 0) {
            int index = 0;
            for (String image : post.getImages()) {
                InputStream in = Encoders.base64toBinary(image);
                String imagePath = InitialConfigurations.POST_IMAGES_PATH + post.getId() + "_" + index + ".jpg";
                FileHelper.writeFile(in, imagePath);
                imagesUrls.add(InitialConfigurations.IMAGES_URL_PATH + "posts/" + post.getId() + "_" + index + ".jpg");
                index++;
            }
            post.setImages(imagesUrls);
            mongoOperation.save(post);
        }

        result.setAsSuccess();
        // TODO move this to a separate helper class after admin implementation of post approval
//        PusherUtil.getOperator().trigger("test_channel", "my_event", Collections.singletonMap("message", "New post available"));
        Activity.stampActivity(Action.POST_PLANT, userId, post);
        return Response.status(200).entity(result.toString()).build();
    }

    @GET
    @Path("/get")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPostsByUser(@QueryParam("offset") int offset) {
        Principal principal = securityContext.getUserPrincipal();
        String userId = principal.getName();
        Result result = new Result();
        List<Post> postList = new ArrayList<>();

        Query query = new Query(Criteria.where("postedById").is(userId));
        query.skip(offset);
        query.limit(InitialConfigurations.PAGINATION_PAGE_SIZE);
        query.with(new Sort(Sort.Direction.DESC, "timestamp"));

        postList = mongoOperation.find(query, Post.class);
        result.setAsSuccess();
        result.addData("offset", offset);
        result.addData("posts", postList);

        return Response.status(200).entity(result.toString()).build();
    }

    @GET
    @Path("/get-all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllPosts(@QueryParam("offset") int offset) {
        Result result = new Result();
        List<Post> postList;

        Query query = new Query(Criteria.where("state").is(Post.State.SAVED));
        query.skip(offset * InitialConfigurations.PAGINATION_PAGE_SIZE);
        query.limit(InitialConfigurations.PAGINATION_PAGE_SIZE);
        query.with(new Sort(Sort.Direction.DESC, "timestamp"));

        postList = mongoOperation.find(query, Post.class);
        result.setAsSuccess();
        result.addData("offset", offset);
        result.addData("posts", postList);

        return Response.status(200).entity(result.toString()).build();
    }

    @SuppressWarnings("Duplicates")
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletePost(Post post) {
        Result result = new Result();
        Post dbPost = mongoOperation.findOne(Query.query(Criteria.where("_id").is(post.getId())), Post.class);

        // deleting all the images
        if (dbPost != null && dbPost.getImages() != null && dbPost.getImages().size() > 0) {
            int index = 0;
            for (String image : dbPost.getImages()) {
                String imagePath = InitialConfigurations.POST_IMAGES_PATH + post.getId() + "_" + index + ".jpg";
                FileHelper.deleteFile(imagePath);
                index++;
            }
        }

        mongoOperation.remove(post);
        result.setAsSuccess();
        return Response.status(200).entity(result.toString()).build();
    }

    @POST
    @Path("/edit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editPost(String json) {
        // TODO not implemented
        Result result = new Result();
        Genson genson = new GensonBuilder()
                .useConstructorWithArguments(true)
                .create();
        Post post = genson.deserialize(json, Post.class);

        mongoOperation.save(post);
        result.setAsSuccess();
        return Response.status(200).entity(result.toString()).build();
    }

    private void uploadImages(final Post post) {
        mongoOperation.save(post);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int index = 1;
                    InputStream in;
                    List<String> urls = new ArrayList<String>();
                    if (post.getImages() != null) {
                        for (String i : post.getImages()) {
                            in = Encoders.base64toBinary(i);
                            String url = DbxHelper.uploadToDbx(in, InitialConfigurations.DBX_POST_IMAGES_PATH, post.getId()
                                    + "_"
                                    + index
                                    + "_post.jpeg");
                            urls.add(url);
                        }
                        post.setImages(urls);
                        mongoOperation.save(post);
                    }
                    return;
                } catch (DbxException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

}

package com.dvios.rest.controllers;

import com.dropbox.core.DbxException;
import com.dvios.rest.ActivityLogger.Action;
import com.dvios.rest.ActivityLogger.Activity;
import com.dvios.rest.Util.Encoders;
import com.dvios.rest.config.InitialConfigurations;
import com.dvios.rest.controllers.auth.Secured;
import com.dvios.rest.handlers.TokenHandler;
import com.dvios.rest.helpers.DbxHelper;
import com.dvios.rest.helpers.FileHelper;
import com.dvios.rest.models.collections.Complaint;
import com.dvios.rest.models.collections.Event;
import com.dvios.rest.models.foundation.Result;
import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author dulaj
 * @version 1.0.0
 */

@Secured
@Path("complaint")
public class ComplaintController extends AbstractController {

    @Context
    SecurityContext securityContext;

    private static final Logger logger = LogManager.getLogger();

    @PUT
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(String json) {
        Principal principal = securityContext.getUserPrincipal();
        String userId = principal.getName();
        Result result = new Result();

        Genson genson = new GensonBuilder()
                .useConstructorWithArguments(true)
                .useDateFormat(new SimpleDateFormat("yyyy-MM-dd hh:mm a"))
                .create();
        Complaint complaint = genson.deserialize(json, Complaint.class);

        complaint.setPostedById(userId);
        mongoOperation.insert(complaint);

        List<String> imagesUrls = new ArrayList<>();
        if (complaint.getImages() != null && complaint.getImages().size() > 0) {
            int index = 0;
            for (String image : complaint.getImages()) {
                InputStream in = Encoders.base64toBinary(image);
                String imagePath = InitialConfigurations.COMPLAINT_IMAGES_PATH + complaint.getId() + "_" + index + ".jpg";
                FileHelper.writeFile(in, imagePath);
                imagesUrls.add(InitialConfigurations.IMAGES_URL_PATH + "complaints/" + complaint.getId() + "_" + index + ".jpg");
                index++;
            }
            complaint.setImages(imagesUrls);
            mongoOperation.save(complaint);
        }

        result.setAsSuccess();
        Activity.stampActivity(Action.POST_COMPLAINT, userId, complaint);
        return Response.status(200).entity(result.toString()).build();
    }

    @GET
    @Path("/get")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getComplaintsByUser(@QueryParam("offset") int offset) {
        Principal principal = securityContext.getUserPrincipal();
        String userId = principal.getName();
        Result result = new Result();
        List<Complaint> complaintList;

        Query query = new Query(Criteria.where("postedById").is(userId));
        query.skip(offset);
        query.limit(InitialConfigurations.PAGINATION_PAGE_SIZE);
        query.with(new Sort(Sort.Direction.DESC, "timestamp"));

        complaintList = mongoOperation.find(query, Complaint.class);
        result.setAsSuccess();
        result.addData("offset", offset);
        result.addData("complaints", complaintList);

        return Response.status(200).entity(result.toString()).build();
    }

    @GET
    @Path("/get-all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllComplaints(@QueryParam("offset") int offset) {
        Result result = new Result();
        List<Complaint> complaintList;

        Query query = new Query();
        query.skip(offset);
        query.limit(InitialConfigurations.PAGINATION_PAGE_SIZE);
        query.with(new Sort(Sort.Direction.DESC, "timestamp"));

        complaintList = mongoOperation.find(query, Complaint.class);
        result.setAsSuccess();
        result.addData("offset", offset);
        result.addData("complaints", complaintList);

        return Response.status(200).entity(result.toString()).build();
    }

    @SuppressWarnings("Duplicates")
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletePost(Complaint complaint) {
        Result result = new Result();

        Complaint dbComplaint = mongoOperation.findOne(Query.query(Criteria.where("_id").is(complaint.getId())), Complaint.class);

        // deleting all the images
        if (dbComplaint != null && dbComplaint.getImages() != null && dbComplaint.getImages().size() > 0) {
            int index = 0;
            for (String image : dbComplaint.getImages()) {
                String imagePath = InitialConfigurations.COMPLAINT_IMAGES_PATH + dbComplaint.getId() + "_" + index + ".jpg";
                FileHelper.deleteFile(imagePath);
                index++;
            }
        }

        mongoOperation.remove(complaint);
        result.setAsSuccess();
        return Response.status(200).entity(result.toString()).build();
    }

    @POST
    @Path("/edit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editPost(String json) {
        Result result = new Result();
        Genson genson = new GensonBuilder()
                .useConstructorWithArguments(true)
                .create();
        Complaint complaint = genson.deserialize(json, Complaint.class);

        mongoOperation.save(complaint);
        result.setAsSuccess();
        return Response.status(200).entity(result.toString()).build();
    }
}

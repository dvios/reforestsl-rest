package com.dvios.rest.controllers;

import com.dropbox.core.DbxException;
import com.dvios.rest.Util.Encoders;
import com.dvios.rest.Util.MongoUtil;
import com.dvios.rest.config.InitialConfigurations;
import com.dvios.rest.helpers.DbxHelper;
import com.dvios.rest.models.collections.Event;
import com.dvios.rest.models.collections.Post;
import com.dvios.rest.models.collections.User;
import com.mongodb.*;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anuradhawick on 11/13/16.
 */
@Path("/migrate")
public class migrations {
    @GET
    public Response migrateAll() throws UnknownHostException {
        // migrating post images
//        migrate();
        return Response.status(Response.Status.OK).entity("NOT ALLOWED").build();

    }

    public static void migrate() throws UnknownHostException {
        MongoOperations ops = MongoUtil.getOperator();
        DBCollection collection;
        DB db;
        MongoClient mongoClient;
        mongoClient = new MongoClient("localhost", 27017);
        db = mongoClient.getDB("reforestsl");
        collection = db.getCollection("users");
        DBCursor dbCursor = collection.find();
        while (dbCursor.hasNext()) {
            DBObject obj = dbCursor.next();
            // convert all encodedImage type to images array
            String value = obj.get("image") == null ? null : (String) obj.get("image");
            String id = obj.get("_id").toString();
            User user = ops.findOne(Query.query(Criteria.where("_id").is(id)), User.class);
            if (value == null) continue;
            try {
                InputStream in;
                List<String> urls = new ArrayList<String>();
                if (value != null) {
                    in = Encoders.base64toBinary(value);
                    String url = DbxHelper
                            .uploadToDbx(in, InitialConfigurations.DBX_USER_IMAGE_PATH, user.getEmail() + "_user.jpeg");
                    user.setImage(url);
                    ops.save(user);
                }
            } catch (DbxException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

package com.dvios.rest.controllers;

import com.dvios.rest.config.InitialConfigurations;
import com.dvios.rest.controllers.auth.Secured;
import com.dvios.rest.models.collections.Event;
import com.dvios.rest.models.collections.Post;
import com.dvios.rest.models.foundation.Result;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.List;

/**
 * @author anuradhawick on 10/2/16.
 * @version 1.0.0
 */

@Secured
@Path("/news-feed")
public class NewsController extends AbstractController{

    @Context
    SecurityContext securityContext;

    /*
    * No Authorization is required. Approved posts should be publicly available
    * */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNews(@QueryParam("offset") int offset) {
        Result result = new Result();
        List<Post> posts;
        List<Event> events;

        Query query = new Query(Criteria.where("state").is(Post.State.SAVED));
        query.skip(offset * InitialConfigurations.PAGINATION_PAGE_SIZE);
        query.limit(InitialConfigurations.PAGINATION_PAGE_SIZE);
        query.with(new Sort(Sort.Direction.DESC, "timestamp"));

        posts = mongoOperation.find(query, Post.class);

        query = new Query(Criteria.where("state").is(Event.State.SAVED));
        query.skip(offset * InitialConfigurations.PAGINATION_PAGE_SIZE);
        query.limit(InitialConfigurations.PAGINATION_PAGE_SIZE);
        query.with(new Sort(Sort.Direction.DESC, "timestamp"));

        events = mongoOperation.find(query, Event.class);

        result.addData("offset", offset);
        result.addData("posts", posts);
        result.addData("events", events);
        result.setAsSuccess();
        return Response.status(200).entity(result.toString()).build();
    }
}

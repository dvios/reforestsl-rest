package com.dvios.rest.controllers;

import com.dvios.rest.Util.MongoUtil;
import com.dvios.rest.models.foundation.Result;
import org.springframework.data.mongodb.core.MongoOperations;

import javax.ws.rs.core.Response;

/**
 * @author Dulaj
 * @version 1.0.0
 */
abstract class AbstractController {

    MongoOperations mongoOperation = MongoUtil.getOperator();

    Response returnError(Result result, String errorMsg) {
        result.setAsError(errorMsg);
        return Response.status(200).entity(result.toString()).build();
    }
}
